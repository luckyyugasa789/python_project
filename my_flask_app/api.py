from flask import Flask, request, jsonify, make_response
from flask_sqlalchemy import SQLAlchemy
from werkzeug.security import generate_password_hash, check_password_hash

app = Flask(__name__)

app.config['SECRET_KEY'] = 'secret'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite://///home/lucky/Documents/my_flask_app/apis.db'

db = SQLAlchemy(app)

class Login(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    email = db.Column(db.String(100))
    mobile = db.Column(db.Integer)
    password = db.Column(db.String(80))

@app.route('/user', methods=['GET'])
def get_all_users():

    users = Login.query.all()

    output = []

    for user in users:
        user_data = {}
        user_data['name'] = user.name
        user_data['email'] = user.email
        user_data['mobile'] = user.mobile
        user_data['password'] = user.password
        output.append(user_data)

    return jsonify({'users' : output})

@app.route('/user/<mobile>', methods=['GET'])
def get_one_user(mobile):

    user = Login.query.filter_by(mobile=mobile).first()

    if not user:
        return jsonify({'message' : 'No user found!'})
    user_data = {}
    user_data['name'] = user.name
    user_data['email'] = user.email
    user_data['mobile'] = user.mobile
    user_data['password'] = user.password

    return jsonify({'user' : user_data})

@app.route('/user', methods=['POST'])
def create_user():
    data = request.get_json()

    hashed_password = generate_password_hash( data['password'],method='sha256')

    new_user = Login(name=data['name'], email=data['email'], mobile=data['mobile'], password=hashed_password)
    db.session.add(new_user)
    db.session.commit()

    return jsonify({'message' : 'New user created!'})

@app.route('/login')
def login():
    auth = request.authorization

    if not auth or not auth.username or not auth.password:
        return make_response('Could not verified')

    user = Login.query.filter_by(mobile=auth.username).first()

    if not user:
        return make_response('Could not verified')

    if check_password_hash(user.password, auth.password):
        user_data = {}
        user_data['name'] = user.name
        user_data['email'] = user.email
        user_data['mobile'] = user.mobile
        user_data['password'] = user.password

        return jsonify({'user': user_data})

    return make_response('Could not verified')

if __name__ == '__main__':
    app.run(debug=True)
